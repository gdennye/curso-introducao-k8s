Curso de Introdução ao Kubernetes

Aprenda os principais conceitos do Kubernetes na prática através de um projeto que servirá de guia em todo o conteúdo.

Neste curso você irá aprender os principais fundamentos do Kubernetes e estar preparado em utilizar essa poderosa plataforma em seus projetos, além de aprimorar seus conhecimentos de forma TOTALMENTE PRÁTICA, utilizando uma das mais famosas plataformas de conteúdo existentes no mercado o WordPress.

Quais são os principais tópicos que serão aprendidos do Kubernetes ?

    Pod

    Deployment

    Services

    Secrets

    Volumes

    AutoScale

Porque utilizar o WordPress como projeto base ?

O Wordpress é uma das mais famosas plataformas de conteúdo existente no mercado, e ocupa mais de 61% do mercado de CMS(Content Management System), o que torna uma plataforma bem eficiente e muito utilzada no mercado.

Os conceitos aprendidos neste curso, poderão ser aplicados nas principais Cloulds(Amazon, Azure e Google Clould) ?
Sim, com toda a certeza, todos os comandos e conceitos aprendidos na aplicação do projeto prático são totalmente compativeis com as principais Cloulds.

Como esse curso pode me ajudar a conhecer um pouco mais sobre Kubernetes ?
O objetivo deste curso introdutório e trazer para você um conhecimento e aprendizado base que poderá te ajudar a aplicar os principais conceitos e comandos necessários para que consiga ter uma aplicação dentro do Kubernetes e de forma escalável e segura.
O que você aprenderá

    Fundamentos principais do Kubernetes
    Implementar um projeto prático no Kubernetes
    Configuração e instalar o Minikube e Kubectl
    Entender e aplicar um Deployment
    Entender e Aplicar Services
    Entender e Aplicar Secrets
    Entender e Aplicar Volumes
    Estressar um ambiente e configurar o AutoScale

Há algum requisito ou pré-requisito para o curso?

    Visual Studio Code
    Sistema Operacional Windows

Para quem é este curso:

    Para quem gosta de aprender e conhecer sobre a tecnologia quem tem revolucionado o mundo de desenvolvimento e infraestrutura.
    São visionárias e entendem que o universo de Containers e Kubernetes é um caminho sem volta e necessita estar antenado nessa onda.
    Quer aprender de forma prática e no final ter uma aplicação WordPress escalavél em um ambiente Kubernetes.
    Conhecer de forma introdutória como funciona a arquitetura do Kubernetes.
    Configurar de forma segura o armazenamento de informações sensiveis dentro do Kubernetes de forma segura.